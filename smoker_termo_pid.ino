#include "max6675.h"
#include <Servo.h>
#include <AutoPID.h>
#include "pitches.h"
bool use_servo = false;
Servo fan_servo;
int fan_pin = 9;

int thermoDO = 4;
int thermoCS = 5;
int thermoCLK = 6;
int vccPin = 3;
int gndPin = 2;
MAX6675 thermocouple(thermoCLK, thermoCS, thermoDO);
unsigned long currentMillis;
unsigned long last_beep_millis = 0;

//parmas:
double target_temp = 108;
#define OUTPUT_MIN 10
#define OUTPUT_MAX 30
#define KP 3
#define KI 0.01
#define KD 0
#define sample_interval 2000
#define bang_bang 40
int bip_interval = 3000;

double temp, setPoint, outputVal,last_outputVal;
AutoPID myPID(&temp, &target_temp, &outputVal, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);
void setup() {
  
  Serial.begin(9600);
  Serial.println("hello!");
  if (use_servo==true){
    fan_servo.attach(fan_pin);
  }
  else {
    pinMode(fan_pin, OUTPUT);
  }
  pinMode(vccPin, OUTPUT); digitalWrite(vccPin, HIGH);
  pinMode(gndPin, OUTPUT); digitalWrite(gndPin, LOW);
  myPID.setBangBang(8,2); 
  myPID.setTimeStep(sample_interval);
}

void loop() {
  currentMillis  = millis();
  temp = thermocouple.readCelsius();
  myPID.run();

  if (use_servo == true){
    fan_servo.write(int(outputVal));
  }
  else {
    analogWrite(fan_pin, int(outputVal*2.833));
  }
   
  for (int i=0;i<temp/3;i++)
      Serial.print(" ");
  Serial.println("*");
  for (int i=0;i<target_temp/3;i++)
      Serial.print(" ");
  Serial.print("|                  ");
  Serial.print("power = ");
  Serial.print(outputVal);
  Serial.print("/30, C = ");
  Serial.println(temp);

  if ((temp > target_temp*1.05) && (currentMillis-last_beep_millis > bip_interval)) {
    tone(8, NOTE_A3, 100);
    delay(100);
    tone(8, NOTE_C4, 200);
    last_beep_millis = millis();
  }
  
  else if ((temp < target_temp*0.95) && (currentMillis-last_beep_millis > bip_interval)) {
    tone(8, NOTE_C4, 100);
    delay(100);
    tone(8, NOTE_A3, 200);
    last_beep_millis = millis();
  }  
  delay(200);
}


